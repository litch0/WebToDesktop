using System;
using System.Threading.Tasks;
using Gtk;
using SharpWebview;
using SharpWebview.Content;

namespace WebToDesktop.Forms
{
    public class BrowserWindow
    {
        public Webview WebContent;
        public string Url
        {
            get => Url;
            set
            {
                if (value == null) throw new ArgumentNullException(nameof(value));
                WebContent.Navigate(new UrlContent(value));
                Url = value;
            }
        }
        
        public BrowserWindow(string url, int width, int height)
        {
            Console.WriteLine(1);
            url = FormatUrl(url);
            Console.WriteLine(1);
            WebContent = new SharpWebview.Webview();
            Console.WriteLine(1);
            WebContent.SetSize(width, height, WebviewHint.None);
            Console.WriteLine(1);
            WebContent.Navigate(new UrlContent(url));
        }

        public void Show() => WebContent.Run();

        public void ShowAsync() => Task.Run(async () => Task.Run( () => WebContent.Run()));

        public void SetTitle(string title) => WebContent.SetTitle(title);

        private string FormatUrl(string url)
        {
            if (url.StartsWith("https://"))
                url = url.Replace("https://", "http://");
            if (url.Contains("localhost/"))
                url = url.Replace("localhost/", $"localhost:{Manager.WebPort}/");
            if (url.StartsWith("/"))
                url = $"http://localhost:{Manager.WebPort}{url}";
            Console.WriteLine(url);

            return url;
        }
    }
}