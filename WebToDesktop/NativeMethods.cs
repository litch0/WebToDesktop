using System;
using System.Runtime.InteropServices;

namespace WebToDesktop
{
    public class NativeMethods
    {
        const string libwebkit = "libwebkit2gtk-4.0.so.37";
        
        [DllImport(libwebkit, CallingConvention = CallingConvention.Cdecl)]
		public extern static IntPtr webkit_web_view_new();

		[DllImport(libwebkit, CallingConvention = CallingConvention.Cdecl)]
		public extern static void webkit_web_view_load_uri(IntPtr web_view, string uri);

		[DllImport(libwebkit, CallingConvention = CallingConvention.Cdecl)]
		public extern static string webkit_web_view_get_uri(IntPtr web_view);

		[DllImport(libwebkit, CallingConvention = CallingConvention.Cdecl)]
		public extern static void webkit_web_view_load_html(IntPtr web_view, string content, string base_uri);

		[DllImport(libwebkit, CallingConvention = CallingConvention.Cdecl)]
		public extern static string webkit_web_view_get_title(IntPtr web_view);

		[DllImport(libwebkit, CallingConvention = CallingConvention.Cdecl)]
		public extern static void webkit_web_view_reload(IntPtr web_view);

		[DllImport(libwebkit, CallingConvention = CallingConvention.Cdecl)]
		public extern static void webkit_web_view_stop_loading(IntPtr web_view);

		[DllImport(libwebkit, CallingConvention = CallingConvention.Cdecl)]
		public extern static bool webkit_web_view_can_go_back(IntPtr web_view);

		[DllImport(libwebkit, CallingConvention = CallingConvention.Cdecl)]
		public extern static void webkit_web_view_go_back(IntPtr web_view);

		[DllImport(libwebkit, CallingConvention = CallingConvention.Cdecl)]
		public extern static bool webkit_web_view_can_go_forward(IntPtr web_view);

		[DllImport(libwebkit, CallingConvention = CallingConvention.Cdecl)]
		public extern static void webkit_web_view_go_forward(IntPtr web_view);

		[DllImport(libwebkit, CallingConvention = CallingConvention.Cdecl)]
		public extern static void webkit_web_view_run_javascript(IntPtr web_view, string script, IntPtr cancellable, Delegate callback, IntPtr user_data);

		[DllImport(libwebkit, CallingConvention = CallingConvention.Cdecl)]
		public extern static IntPtr webkit_web_view_run_javascript_finish(IntPtr web_view, IntPtr result, IntPtr error);

		[DllImport(libwebkit, CallingConvention = CallingConvention.Cdecl)]
		public extern static IntPtr webkit_javascript_result_get_global_context(IntPtr js_result);

		[DllImport(libwebkit, CallingConvention = CallingConvention.Cdecl)]
		public extern static IntPtr webkit_javascript_result_get_value(IntPtr js_result);

		[DllImport(libwebkit, CallingConvention = CallingConvention.Cdecl)]
		public extern static IntPtr JSValueToStringCopy(IntPtr context, IntPtr value, IntPtr idk);

		[DllImport(libwebkit, CallingConvention = CallingConvention.Cdecl)]
		public extern static int JSStringGetMaximumUTF8CStringSize(IntPtr js_str_value);

		[DllImport(libwebkit, CallingConvention = CallingConvention.Cdecl)]
		public extern static void JSStringGetUTF8CString(IntPtr js_str_value, IntPtr str_value, int str_length);

		[DllImport(libwebkit, CallingConvention = CallingConvention.Cdecl)]
		public extern static void JSStringRelease(IntPtr js_str_value);

		[DllImport(libwebkit, CallingConvention = CallingConvention.Cdecl)]
		public extern static IntPtr webkit_navigation_policy_decision_get_request(IntPtr decision);
		
		[DllImport(libwebkit, CallingConvention = CallingConvention.Cdecl)]
		public extern static string webkit_uri_request_get_uri(IntPtr request);
	}

    public static class WebviewgtkWindows
    {
	    private static CallingConvention mainCallingConvention = CallingConvention.Cdecl;
	    private const string libwebkitdll = "libwebkitgtk-3.0-0.dll";
	    [DllImport(libwebkitdll, CallingConvention = CallingConvention.Cdecl)]
	    public extern static IntPtr webkit_web_view_new();

	    [DllImport(libwebkitdll, CallingConvention = CallingConvention.Cdecl)]
	    public extern static void webkit_web_view_load_uri(IntPtr web_view, string uri);
    }
}