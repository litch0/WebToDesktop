# WebToDesktop
.Net Core Library for making User Interfaces with Web Technology such as Blazor, MVC and Razor pages. 
Working With GtkSharp and WebKit(servo in the Future)

# How to use?
download the source code and reference the WebToDesktop project in your ASP.NET Core web app
after that in the program.cs:
```c#
public class Program{
    static void Main(string[] args) => Manager.RunServer<Startup>("org.app.id");
}
```

in your Startup services:
```c#
public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
{
   // basic code of the template
    MainWindow = new BrowserWindow("/");
    MainWindow.SetDefaultSize(1000, 800);
    
    Manager.app.AddWindow(MainWindow);
    MainWindow.ShowAll();
}
```


# What is It?
The Library combines 2 powerful technology to make GUIs with web technology.

# How it works?
We first create a Host builder that will handle all the backend webhost part.
After that we initialize an Gtk app that will handle the front-end and backend of the app.

# Supported Platforms
`Linux   : Gtk and Webkit`

`MacOS   : Gtk and Webkit`

`Windows : Gtk and Webkit (NOT SUPPORTED NOW)`

# Next Steps
I plan on replacing the Webkit with the Servo, but It's very dificult so It may take a while, you can see the progress [here](https://github.com/servo/servo/discussions/27595)
