using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using WebToDesktop;

namespace TestApp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Manager.RunServer<Startup>("org.test.app");
        }
    }
}